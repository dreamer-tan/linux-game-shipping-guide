---
title: "Seeking Help"
category: Further Help
order: 6
---
If you have any further questions or concerns not addressed in this guide,
please reach out to the community. The following places are recommended for
help:

* Reddit: [r/linux_gaming](https://www.reddit.com/r/linux_gaming/), [r/gamedev](https://www.reddit.com/r/gamedev/)
* [GamingOnLinux](gamingonlinux.com)
