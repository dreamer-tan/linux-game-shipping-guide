---
title: "Leverage Linux Loyalty"
category: Advice
order: 4
---
The Linux gaming platform has a uniquely high level of platform loyalty compared
to other platforms. As a game developer, you should absolutely take advantage of
that 'Linux Loyalty' and leverage it to increase the sales of your game.

To best take advantage of that Linux Loyalty, you should make sure to let the
Linux gaming community know about your game and it's Linux support to maximise
sales.

Many Linux gaming focused websites, frequently advertise new release Linux games
to their many visitors, and are a great form of promotion for your games. One
such website is [gamingonlinux.com](https://gamingonlinux.com), run by Liam Dawe
(who can be contacted via the website or [GamingOnLinux Discord Group](https://discord.gg/0rxBtcSOonvGzXr4)).

(🔨 *More websites?*)

Consider requesting your social media campaigner create a post on r/linux_gaming
when you release your game, to let the community know, for more great promotion
of your title.

(🔨 *Anywhere else?*)
