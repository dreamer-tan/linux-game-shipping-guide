---
title: "Proton Compatibility"
category: Advice
order: 5
---
If after exploring all options you find that Linux support is still
prohibitively difficult, time consuming, and/or expensive, there is the option
of ensuring that your game is compatible with Proton.

Proton is a Compatibility Tool created by Valve that makes it possible for Linux
gamers to play many Windows games on Linux with, varying degrees of success.
While this is an advantage particularly for games that release on Steam, it is
not necessary for your game to be available on Steam to be played with Proton.

Generally speaking there is not much that you as a developer are required to do
to take advantage of Proton. Proton compatibility when it occurs without
intention doesn't cost anything and only expands the reach of your game,
potentially providing additional sales.

Statistically speaking, you are likely to find your game will already be
compatible with Proton even without making an attempt to officially support it,
as at the time of writing around 60% of Steam's games are compatible with
Proton.

Maintaining Proton compatibility is less a matter of taking specific steps, and
more a list of technologies you should avoid to maintain compatibility with
Proton, mainly the following:

* Intrusive DRM
* Anti-cheat middleware
* Windows Media Foundation APIs
* .Net Framework
* WMA and WMV media formats
  * These formats are patented by Microsoft, so while there are ways to make
  these formats work in Proton (see: ffmpeg), Valve is not legally allowed to
  distribute builds of Proton where this is possible.
  * As alternatives, you may want to consider using [Vorbis](https://xiph.org/vorbis/)
  and [Theora](https://www.theora.org/), respectively. These media formats work
  great in Proton.

For optional performance, it is recommended to use Vulkan as your primary
renderer *(or at least an option in your game's graphics settings)*.

[The official advice from Valve on this subject is:](https://steamcommunity.com/games/221410/announcements/detail/1696055855739350561)

>*"We recommend you target Vulkan natively in order to offer the best possible
performance on all platforms, or at least offer it as an option if possible.
It's also a good idea to avoid any invasive third-party DRM middleware, as they
sometimes prevent compatibility features from working as intended."*

The great advantage of Proton compatibility, is the expanded reach of your game
to a slightly larger audience, without any additional burden of support. Small
decisions during your game development on how to avoid losing compatibility with
Proton early on, can help achieve 'Platinum' (meaning that your game runs as
well, or better, than on Windows) Proton compatibility, resulting in more sales.

A catch of relying on Proton is the loyalty of many Linux gamers to their
platform. Some Linux gamers will insist on only purchasing native Linux games
for their platform, so called *'no tux, no bucks'* policy. To achieve the
highest number of sales possible and take full advantage of the Linux market, a
native Linux title is still necessary.

Another thing worth mentioning is that Proton currently is not enabled by
default in Steam and requires the user to manually take initiative and enable it
in Steam's settings. Even then, by default Proton will only be enabled for Valve
and developer approved games, and requires an additional toggle to enable it for
all titles. This will most likely change to some degree when Proton leaves beta,
but as it currently stands this is how the system functions. With that said,
however, Proton has made huge waves within the Linux gaming community, and its
existence is certainly no secret at this point.
