---
title: "Benefits of Linux Support"
category: Why Support Linux?
order: 1
---
Despite being often seen as an additional burden, developing games for multiple
platforms, including Linux, has many advantages.

**Portable Code is Better Code**

Different operating systems may react differently to the same code. A bug or
non-standard use of C/C++ can produce errors that may be swallowed in one
moment but emerge somewhere else. In some instances, a bug that rarely appears
on one platform can be easily reproducible on another platform. Compiling and
running your game on multiple compilers and OSes can strengthen a codebase, and
make it more robust.

Not only can multiplatform support make your code more stable, but it can also
improve performance, [as Valve discovered themselves](http://blogs.valvesoftware.com/linux/faster-zombies/)
when they ported their first game to Linux, Left 4 Dead 2. During the process of
porting their game and engine, what they learnt revealed issues with their
Windows version of their engine as well.

Multiplatform development is generally only difficult if your code is tied very
tightly to a specific platform. By ironing out the bugs of multiplatform support
and developing a robust multiplatform workflow, you eliminate bugs and
inefficiencies in supporting multiple platforms and create truly portable code.
With portable code, it becomes easy to support additional platforms in the
future, such as new game consoles or cloud streaming services (such as Google
  Stadia, a Linux based platform).

**Increased Sales**

[It goes without saying.](https://twitter.com/grumpygamer/status/858387467187101696)
When the cross-platform work is managed efficiently, multiplatform support
simply means more sales for your game. Different games appeal to the Linux
gaming market to varying degrees, for some games Linux support could be a large
boost to your sales figures for minimal extra work.

**Pro-Consumer Positive Image**

Linux support is seen favourably as a positive thing by consumers, like a game
promoting DRM-free as a feature. Linux support on Steam is also a great
'value-added' benefit to your game even for your Windows customers, as the Linux
version of your game is included in their purchase.
