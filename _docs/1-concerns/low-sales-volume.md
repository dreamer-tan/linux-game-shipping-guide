---
title: "Low Sales Volume"
category: Concerns
order: 2
---

**"The number of Linux gamers is a tiny fraction of the overall market, so I
won't receive a high enough number of sales to warrant bothering to release a
native Linux port."**

Ultimately the decision is for you the developer to make, to decide if it is or
isn't worth the effort to create a native Linux release. No one will deny that
the overall number of Linux gamers compared to Windows gamers is significantly
lower.

According to [Valve's Steam Hardware Survey,](https://store.steampowered.com/hwsurvey)
at the time of writing, there are an estimated 684,000 monthly active users who use
Linux as their primary desktop OS on Steam. This number is calculated by taking
the 0.76% userbase statistic from the June 2019 survey, and applying that % to
the estimated 90 million 'monthly active users' that
[Valve reports](https://steamcommunity.com/groups/steamworks/announcements/detail/1697194621363928453)
to have.

Linux gamers exert incredibly strong brand loyalty to their platform, are very
pro-sumer in their purchases, and always eager to throw money and support to
developers who support their platform.

By its nature, the Linux gaming market also has much less competition and
saturation than the Windows gaming market, due to the overall lower number of
available games.

You may find your title will be especially popular if your game is of a genre
that isn't commonly available on Linux. AAA games and multiplayer PvP games are
often not available on Linux either natively or via Proton *(due to their
frequent use of AntiCheat technology that isn't compatible with Proton)*. If
either of those descriptions fit your game, you should consider that your title
would not be facing much competition on Linux.
