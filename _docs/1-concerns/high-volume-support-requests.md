---
title: "High Volume Support Requests"
category: Concerns
order: 1
---

**"I've heard unpleasant stories from other developers about supporting Linux,
such as the disproportionally high volume of support requests."**

Some developers have indeed had bad experiences in shipping games to Linux.
However, many developers have expressed very positive experiences as well.

That said, there is a basis to the claims of negative experiences in the past, and
of support requests from Linux users being noticeable *different* to the support
requests from Windows and Mac OS users. In past years, Linux desktop OSes had a
vastly different level of official driver support from Intel, AMD and NVIDIA.
Official support from game engines, if there was any at all, was much poorer.
Steam's Linux support has improved in the past several years. Anyone will tell
you, a few years is a long time in the world of technology, and things have
changed substantially in recent times.

It is also true that Linux is very different to Windows, and if applications
are distributed the wrong way to Linux users by developers inexperienced with
the platform, that there can be issues with missing libraries and poor desktop
integration, however those issues can be easily avoided *(and how will be
described further on in this guide).*

Overshadowing those two points is the biggest reason why there is some truth to
the claim that you will receive disproportionately higher numbers of reports
from Linux users: *Linux users are more likely to report issues with software
than non-Linux users*.

It is a fair observation to make that a Linux user is more likely to be both
technically adept, and strongly supportive of open-source software than a
non-Linux user. Supporters of open-source software (and Linux users in general)
typically are used to having a very close relationship with developers compared
to regular software users, often spending time visiting the Githubs of software
they use, leaving detailed bug reports and attempt to reach out directly to a
developer to offer helpful information to improve the quality of software.

[Ana Betts, provided an amusing story of their experience in shipping software to Linux users:](https://mobile.twitter.com/anaisbetts/status/1082361023653326854)

> As a contrasting anecdote, Slack shipped a Linux app based on Electron, it
accounted for a similar % of users as global OS share, and a similar amount of
tickets.
>
> In contrast though, the ticket *quality* of Linux users was through the roof -
they were super clear and actionable.
>
> ...
>
> Linux users were also *super* understanding that their weird config wasn't
supported, especially if you gave them Real Technical Info back in your support
response. They often would send us callstacks *and* a few times, even patch
Electron for us to fix it

So it is likely you will receive more bug reports from Linux users than
non-Linux users, and also likely than the quality of the bug reports you receive
from Linux users will be of a higher quality than average, with more detailed
information. This can be viewed as a positive because often the detailed bug
reports can be useful in identifying issues that affect users across all of your
supported platforms.
