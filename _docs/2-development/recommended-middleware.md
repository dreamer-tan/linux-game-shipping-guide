---
title: "Recommended Middleware"
category: Development
order: 5
---
**SDL**

Quoting the official website:

>Simple DirectMedia Layer is a cross-platform development library designed to
provide low level access to audio, keyboard, mouse,  joystick, and graphics
hardware via OpenGL and Direct3D. It is used by video playback software,
emulators, and popular games including [Valve](http://valvesoftware.com/)'s
award winning catalog and many [Humble Bundle](https://www.humblebundle.com/)
games.

SDL is a library commonly used to simplify the work of creating cross platform
games or game engines, and supports Windows, Mac OS X, Linux, iOS, and Android.
SDL has been in development for a long time and is a highly effective solution
for abstracting the details of how to perform common OS specific tasks. This is
especially useful for porting a game to Linux with an existing codebase written
from scratch.

For more information on how to use SDL, [consult the SDL's official wiki.](https://wiki.libsdl.org/FrontPage)

**Unreal Engine**

Unreal Engine has the ability to compile to native Linux and the Unreal Engine
Editor is available for Linux as well. The Unreal Engine Editor requires an
account on the [Unreal Engine website](https://www.unrealengine.com/en-US/).
After signing up, you will need to follow [these instructions](https://www.unrealengine.com/en-US/ue4-on-github)
to access the [official Epic repositories](https://github.com/EpicGames/Signup),
and to access the [Unreal Engine repository](https://github.com/EpicGames/UnrealEngine).
After following these steps, you can clone the git repository of Unreal Engine
and compile it from source.

**Unity**

Unity features Linux support and can compile native Linux games. Recommended
versions of Unity to use are 2017.2 or 2019.x, and to avoid 2017.3 and 2018.3.
If you are using either version it is recommended to upgrade to 2019.x to
[avoid a Vulkan crash on Linux.](https://issuetracker.unity3d.com/issues/vulkan-build-crashes-on-launch-for-linux)

In 2019, Unity announced they will be
[officially supporting Linux for their editor as well,](https://blogs.unity3d.com/2015/08/26/unity-comes-to-linux-experimental-build-now-available/)
due to increased demand.

**Godot**

Godot is an MIT licensed open source game engine that can be used for free
without any licensing costs. The Godot engine and Godot Editor both support
Linux, and Godot also offers a headless server version of Godot to run on Linux
for multiplayer games. Godot is the perfect choice of engine for 2D games, with
3D support as well, and offers easy 1-click one export to Windows, Mac, Linux,
Android and iOS, with commercial porting services available for supporting game
consoles as well. If your game is developed in Godot, supporting Linux is a
no-brainer.
