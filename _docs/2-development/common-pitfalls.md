---
title: "Common Pitfalls"
category: Development
order: 7
---
**Game Engines That Support Linux Still Require Testing**

Game engines like Unreal Engine and Unity are incredibly helpful. These engines
offer you help for exporting your game to Linux, taking care of the building
and/or packaging process. This greatly reduces the effort needed to get your
game running on Linux, however, it is still necessary to test your game running
on a Linux development machine as you would do on Windows, as there  sometimes
can be Linux-specific problems.

**Don't Mix Crossplatform Engines With Platform Dependent Middleware**

A game engine like Unreal Engine, Unity or Godot gives you incredible
flexibility and portability.. until you use Windows-only Middleware, locking you
to only one platform. Be mindful of what middleware you use to create your game
and always aim for cross platform middleware.

**Test Multiple GPUs**

Much like on Windows, wildly different results for the same games can occur from
difficult GPUs. As a minimum, it is recommended to test at least AMD + Mesa,
NVIDIA and Intel + Mesa.

There is for example, strong circumstantial evidence that NVIDIA GPUs are far
more fault tolerant of bad OpenGL code than other vendors.

**64-bit**

32-bit support is increasingly being phased out of the world of computing, and
this is true on Linux as well.
[The number of 32-bit Linux installations](https://www.gamingonlinux.com/users/statistics)
is now so low that there is no benefit to choosing 32-bit over 64bit, and 32-bit
builds are really unnecessary.

**Where To Store Game Data**

The [XDG Base Directory Specification](https://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html)
in simple terms describes where your software should place files on a user's
Linux PC. Much like on Windows, game configuration data and save game data
should be stored in the appropriate directories to avoid cluttering your user's
`/home` directory. Use `$XDG_DATA_HOME` for savegames and `$XDG_CONFIG_HOME` for
configuration files.

The game engine Unity has it's own behaviour for storing data, and stores all
your game data under `~/.config/unity3d/[Your Game Name]`. This doesn't adhere
to the XDG Base Directory Specification, but is still better than cluttering the
user's `/home` directory.

> Note: On Linux, the **~** character in a filepath refers to the user's 'home'
directory. For a user with the username `bob`, the filepath `~/file` would refer
to `/home/bob/file`.

**Filepath Capitalisation**

It's such a small detail that it's easy to overlook. Windows filepaths are not
case sensitive. Linux filepaths are case sensitive. This can create relatively
minor issues that can break games when porting them to Linux. The general advice
is to pick a filepath capitalisation structure and stick to it, or simply use
lowercase for everything.

**Open File Formats for Audio/Video**

For maximum flexibility to port your game to every platform, it is strongly
advised to use open file formats for audio and video.

(🔨 *Any other common pitfalls?*)
