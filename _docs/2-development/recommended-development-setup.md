---
title: "Recommended Development Setup"
category: Development
order: 6
---
**Cross Platform from the Start**

While it is possible to port a game after it has been finished, that is
certainly doing things the hard way. It's best to start cross platform from the
beginning and use cross-platform libraries and technologies. Examples are SDL,
Vulkan, CMake, crossplatform game engines (like Godot), and middleware.

**Compile for Linux as Soon As Possible**

Test early, test often. Start compiling on Linux as soon as possible, for
maximum benefit. If you compile cross-platform from the start, then each
platform will tend to smoke out bugs and bad assumptions, and this will be a net
benefit for all platforms.

**Linux Distributions To Test**

It is safe to simply test your game against the latest Ubuntu LTS version
(currently Ubuntu 18.04).

With that said, however, you may want to consider testing other popular
distributions as well.
[You can see the currently most popular distributions according to Steam here](https://store.steampowered.com/hwsurvey?platform=linux).

**Testing**

There are two approaches to testing both Windows and Linux. The first option is
to dual boot your testing machine, installing both Windows and Linux alongside
each other. Changing OS then requires a simple reboot to switch to the other OS.

Alternatively, you can perform all your development on one OS, and use a virtual
machine to test the other. For your guest OS have access to your host OS's GPU,
you will need to setup your guest OS VM with 'GPU Passthrough'.

Please note, this comes with the warning that GPU passthrough is not always
reliable, can be difficult to setup, can set off some anticheat middleware, and
doesn't always replicate graphics driver behaviour.

**Docker**

Docker is a software platform that allows for creating self-contained
contrainers of software, that bring with them all of their own libraries,
configuration files, and which communicate via defined channels. Docker
containers can be thought of as 'lightweight virtual machines'. Similar to how a
virtual machine virtualizes (removes the need to directly manage) server
hardware, containers virtualize the operating system of a server. With simple
commands, you can build, start, and stop containers.

Build once, run everywhere. Even on different operating systems.

Docker can be used to build containers for game servers, allowing you to, eg:
develop and test on a local Windows machine then deploy those same containers to
a Linux VPS.

Docker can also be used to build containers that contain your game engine, and
used to compile your game for any OS, from any OS. Using this, it is possible to
setup a continous integration pipeline, where your game is rebuilt with every
'git push' to your repository, and report any failure to compile.
