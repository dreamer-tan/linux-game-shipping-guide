# Linux Game Shipping Guide

This project was created for the purpose of compiling as much useful information
in one location as possible relevant to the topic of shipping PC games to the
Linux platform and supporting those games.

Information relevant to this guide:

 * The benefits of cross platform development and supporting Linux
 * Addressing common concerns about supporting Linux
 * Warnings and advice on how to steer clear of common pitfalls developers
 encounter while supporting Linux
 * Information about the Linux platform that a developer may not know, if their
 primary experience of development is games for Windows.
 * Best practices for developing cross platform games
 * Advice on how to maximise sales of games on the Linux platform
 * Advice on how to maintain or achieve Proton compatibility for existing
 Windows games.

Information not relevant to this guide:

 * 'Why you should use Linux'
 * 'Why Linux is so amazing'
 * Requests for preferred methods of game distribution from
 developers/publishers
 * Debates over technology or claims that can not be backed up by a citation

Lets try to keep this guide on topic so that it reads like a useful guidebook,
and not an advert.

## How to Contribute

This guidebook is open source! Would you like to contribute to it?

Contributions can come in many forms, such as adjustments to the visual
appearances of the website, corrections to spelling errors, or whole new pages
filled to the brim with useful information.

There are a two main ways in which you can contribute:

 * Merge Requests
 * Issues

If you are capable of doing so and familiar with the workflow, a
**Merge Request** is the most direct form of contribution. That allows you to
directly edit the website itself, and submit the changes directly to the
website's repository. However you will need to perform some setup to work
locally on the website in an efficient manner.

Not everyone will be familiar or comfortable with doing so however, so another
option is to send useful tips or information via new **Issues** on this
repository. Feel free to type some content into an Issue and offer it as a
contribution to the guidebook.

## Merge Requests & Contribution Guidelines

Here are a few simple guidelines to follow for contributions to the guide, to
ensure this website is always improving in quality and quantity of content.

 * Proof read your contributions thoroughly before submitting.
 * Only provide relevant information.
 * Communicate via Issues any major changes you wish to make.

To directly contribute to the repository, simply fork our repository, perform
your edits, and offer the edits in the form of a merge request. Take time
and care to edit your contributions for the best grammar, spelling and most
friendly tone possible for any potential readers.

If you wish to make major contributions or changes, such as offering redesigns
of the website, or wish to alter or rearrange pages, etc, it's best to first
discuss those changes by creating an Issue first, so everyone knows what the
plan is and is ready for the changes.

## About This Site

This website uses the
[Jekyll static site generator template](https://gitlab.com/pages/jekyll),
a static site generator, to create the website from the files in this
repository. This allows the pages to use templating and even markdown, to
provide the easiest method possible for editing the content's of the website.
This is deliberate, so that contributors of information can focus on the body of
their content, without having to worry about valid HTML/CSS.

Every time a commit added to the master, the website is regenerated again from
the source files and the website is updated. This regeneration takes a few
moments to complete, minutes at most.

To edit this website locally and see your changes on your local machine first
before commiting changes, you should follow the instructions for installing
Ruby & Jekyll, then read the relevant section for local editing.
Instructions for how to do so are located on the link to the template above.

This site automatically generates pages of the guidebook from the directory
structure of the `_docs` folder.

The header of each page specifies the page title, category, and the order which
the page appears within it's category.

Eg:
```
---
title: "Low Sales Volume"
category: Concerns
order: 2
---
```

The order of the folders themselves decide the order of the categories (hence
  the number prefixes).

[This website is based off the Jekyll theme 'edition'.](http://jekyllthemes.org/themes/edition/)

## Contact Us

In addition to Issues, you can also come chat about this guide directly with us
via our [Discord Group](https://discord.gg/tTHehaP).
